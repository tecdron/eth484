eth484
=======

Driver ROS pour le pilotage de la carte ETH484 (relais uniquement)

Possibilité de configuration poussée avec liste des paramètres dans le dossier launch:

ip_address

port

invert_all

invert_relay1

invert_relay2

invert_relay3

invert_relay4

nom_topic_relay1

nom_topic_relay2

nom_topic_relay3

nom_topic_relay4
