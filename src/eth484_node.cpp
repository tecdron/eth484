// // #include <ecl/threads.hpp>
#include <arpa/inet.h>
#include <boost/thread.hpp>
#include <boost/thread/pthread/mutex.hpp>
#include <errno.h>
#include <netdb.h> 
#include <netinet/in.h> 
#include <../../opt/ros/indigo/include/ros/init.h>
#include <ros/ros.h>
#include <ros/spinner.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <std_msgs/Bool.h>
#include <string.h>
#include <sys/socket.h> 
#include <sys/types.h>
#include <unistd.h>

 
 using namespace std;
 
class ETH484
{
public:
  ETH484();
  boost::mutex *mMut;

  ros::NodeHandle nh_;
  bool bRelay1,bRelay2,bRelay3,bRelay4;
  bool bInvertionAll,bInversionRelay1,bInversionRelay2,bInversionRelay3,bInversionRelay4;
  
  ros::Subscriber relay1_sub_,relay2_sub_,relay3_sub_,relay4_sub_;
  
  void triggerRelay1Callback(const std_msgs::Bool::ConstPtr& bOrder);
  void triggerRelay2Callback(const std_msgs::Bool::ConstPtr& bOrder);
  void triggerRelay3Callback(const std_msgs::Bool::ConstPtr& bOrder);
  void triggerRelay4Callback(const std_msgs::Bool::ConstPtr& bOrder);
  
  void ONRelay(int iNbRelay);
  void OFFRelay(int iNbRelay);
      
  std::string sNomTopicRelay1,sNomTopicRelay2,sNomTopicRelay3,sNomTopicRelay4;
  std::string sAdresseIP;
  int iPort;
  int iTimeoutReception;
  int iNbRelay;
  int sockfd;
  struct sockaddr_in serv_addr;
  struct hostent *server;
  long hostAddr;
  long status;
  char buffer[512];
};

ETH484::ETH484():
iPort(17494),sNomTopicRelay1("relay1"),sNomTopicRelay2("relay2"),sNomTopicRelay3("relay3"),sNomTopicRelay4("relay4"),iTimeoutReception(0),sAdresseIP("192.168.1.18")
{
  mMut = new boost::mutex;

  bInvertionAll = false;
  bInversionRelay1 = false;
  bInversionRelay2 = false;
  bInversionRelay3 = false;
  bInversionRelay4 = false;
  iNbRelay=4;
  
  nh_.param("eth484/invert_all", bInvertionAll, bInvertionAll);
  nh_.param("eth484/invert_relay1", bInversionRelay1, bInversionRelay1);
  nh_.param("eth484/invert_relay2", bInversionRelay2, bInversionRelay2);
  nh_.param("eth484/invert_relay3", bInversionRelay3, bInversionRelay3);
  nh_.param("eth484/invert_relay4", bInversionRelay4, bInversionRelay4);
  
  nh_.param("eth484/ip_address", sAdresseIP, sAdresseIP);
  ROS_INFO("IP:%s",sAdresseIP.c_str());
  nh_.param("eth484/port", iPort, iPort);
  nh_.param("eth484/timeout",iTimeoutReception,iTimeoutReception);
  
  
  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  
  if (sockfd < 0)
  {
    ROS_ERROR("ERROR ouverture socket pour ETH484");
    exit(0);
  }
  bzero((char *) &serv_addr, sizeof(serv_addr));
  

  struct sockaddr_in antelope;
  char *some_addr;

  inet_aton(sAdresseIP.c_str(), &antelope.sin_addr); // store IP in antelope

  some_addr = inet_ntoa(antelope.sin_addr); // return the IP
  //ROS_INFO("TEST %s\n", some_addr); // prints "10.0.0.1"
  
  serv_addr = { 0 };
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr = antelope.sin_addr;
  serv_addr.sin_port = htons(iPort);
  
  if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0)
  {
    ROS_ERROR("ERROR connecting");
    ROS_ERROR("ERROR connecting: %s:%d\n", sAdresseIP.c_str(),iPort);
    exit(0);
  }
  
  if(bInvertionAll == true)
  {
    for( int i=1;i<iNbRelay+1;i++)ONRelay(i);
    bRelay1=true;
    bRelay2=true;
    bRelay3=true;
    bRelay4=true;
  }
  
  if(bInversionRelay1 == true)
  {
    ONRelay(1);
    bRelay1=true;
  }
  
  if(bInversionRelay2 == true)
  {
    ONRelay(2);
    bRelay2=true;
  }
    
  if(bInversionRelay3 == true)
  {
    ONRelay(3);
    bRelay3=true;
  }
  
  if(bInversionRelay4 == true)
  {
    ONRelay(4);
    bRelay4=true;
  }
  
  
  nh_.param("eth484/nom_topic_relay1", sNomTopicRelay1, sNomTopicRelay1);
  nh_.param("eth484/nom_topic_relay2", sNomTopicRelay2, sNomTopicRelay2); 
  nh_.param("eth484/nom_topic_relay3", sNomTopicRelay3, sNomTopicRelay3);
  nh_.param("eth484/nom_topic_relay4", sNomTopicRelay4, sNomTopicRelay4);
    
 
  relay1_sub_ = nh_.subscribe<std_msgs::Bool>(sNomTopicRelay1, 1, &ETH484::triggerRelay1Callback, this);
  relay2_sub_ = nh_.subscribe<std_msgs::Bool>(sNomTopicRelay2, 1, &ETH484::triggerRelay2Callback, this);
  relay3_sub_ = nh_.subscribe<std_msgs::Bool>(sNomTopicRelay3, 1, &ETH484::triggerRelay3Callback, this);
  relay4_sub_ = nh_.subscribe<std_msgs::Bool>(sNomTopicRelay4, 1, &ETH484::triggerRelay4Callback, this);

}


void ETH484::triggerRelay1Callback(const std_msgs::Bool::ConstPtr& bOrder)
{
  if(bOrder->data == true)
  {
    if(bInversionRelay1 == true || bInvertionAll == true)
    {
      OFFRelay(1);
    }
    else
    {
      ONRelay(1);
    }
  }
  else
  {
     if(bInversionRelay1 == true || bInvertionAll == true)
    {
      ONRelay(1);
    }
    else
    {
      OFFRelay(1);
    }
  }
    
}
void ETH484::triggerRelay2Callback(const std_msgs::Bool::ConstPtr& bOrder)
{
  if(bOrder->data == true)
  {
    if(bInversionRelay2 == true || bInvertionAll == true)
    {
      OFFRelay(2);
    }
    else
    {
      ONRelay(2);
    }
  }
  else
  {
     if(bInversionRelay2 == true || bInvertionAll == true)
    {
      ONRelay(2);
    }
    else
    {
      OFFRelay(2);
    }
  }
}

void ETH484::triggerRelay3Callback(const std_msgs::Bool::ConstPtr& bOrder)
{
  if(bOrder->data == true)
  {
    if(bInversionRelay3 == true || bInvertionAll == true)
    {
      OFFRelay(3);
    }
    else
    {
      ONRelay(3);
    }
  }
  else
  {
     if(bInversionRelay3 == true || bInvertionAll == true)
    {
      ONRelay(3);
    }
    else
    {
      OFFRelay(3);
    }
  }
}

void ETH484::triggerRelay4Callback(const std_msgs::Bool::ConstPtr& bOrder)
{
  if(bOrder->data == true)
  {
    if(bInversionRelay4 == true || bInvertionAll == true)
    {
      OFFRelay(4);
    }
    else
    {
      ONRelay(4);
    }
  }
  else
  {
     if(bInversionRelay4 == true || bInvertionAll == true)
    {
      ONRelay(4);
    }
    else
    {
      OFFRelay(4);
    }
  }

}

void ETH484::ONRelay(int iNbRelay)
{
 
  mMut->lock(); 
  std::string sNbRelay =std::to_string(iNbRelay);
  std::string sChaine = ":DOA,"+sNbRelay+"<CR>";
  char buffer[255];//=sChaine.c_str();
    if(write(sockfd,sChaine.c_str(), strlen(sChaine.c_str())) < 0)
    {
	//perror("send()");
      mMut->unlock();
	exit(errno);
    }
  //ROS_INFO("Envoi:%s",&buffer);
  int n =  recv(sockfd,buffer,sizeof(buffer),MSG_DONTWAIT);
  //ROS_INFO("Reception:%s",buffer);
  
  mMut->unlock();
}
void ETH484::OFFRelay(int iNbRelay)
{
  mMut->lock(); 
  std::string sNbRelay =std::to_string(iNbRelay);
  std::string sChaine = ":DOI,"+sNbRelay+"<CR>";
  char buffer[255];//=sChaine.c_str();
    if(write(sockfd,sChaine.c_str(), strlen(sChaine.c_str())) < 0)
    {
	//perror("send()");
      mMut->unlock();
	exit(errno);
    }
  //ROS_INFO("Envoi:%s",&buffer);
  int n =  recv(sockfd,buffer,sizeof(buffer),MSG_DONTWAIT);
  //ROS_INFO("Reception:%s",buffer);
  
  mMut->unlock();
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "eth484");
  ros::MultiThreadedSpinner spinner_(4);                    // Use 4 threads
  ETH484 eth_484;

  spinner_.spin();
}